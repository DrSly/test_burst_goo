﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransmitSms;
using TransmitSms.Models;

namespace TestApp1
{
    class Program
    {
        private static string ApiKey = ConfigurationManager.AppSettings["BurstSmsApiKey"];
        private static string ApiSecret = ConfigurationManager.AppSettings["BurstSmsApiSecret"];
        private static string ApiUrl = ConfigurationManager.AppSettings["BurstSmsApiUrl"];

        static void Main(string[] args)
        {

            var tsw = new TransmitSmsWrapper(ApiKey, ApiSecret, ApiUrl);
            var gooListId = 875025;     // global optout list id will never change

            var respOptouts = tsw.GetList(gooListId, MemberType.All, 1, 10);
            var code = respOptouts.Code;
            var description = respOptouts.Description;

        }
    }
}
