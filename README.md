Hi Maria,

Both Code and Description are null.

Attached is a simple C# test app which demonstrates the issue. 

It would be great if the devs can test it, confirm there is a problem with the code, or maybe something else prevents access to the list…

Global opt-out List Id is 875025. It contains one number (61412028353).

Please populate the App.Config file with valid credentials before testing!